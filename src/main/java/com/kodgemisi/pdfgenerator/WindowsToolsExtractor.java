/*
 * Copyright © 2018 Kod Gemisi Ltd.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * This Source Code Form is “Incompatible With Secondary Licenses”, as defined by
 * the Mozilla Public License, v. 2.0.
 */

package com.kodgemisi.pdfgenerator;

import lombok.extern.slf4j.Slf4j;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on April, 2018
 *
 * @author destan
 */
@Slf4j
public class WindowsToolsExtractor extends AbstractToolsExtractor {

	@Override
	protected URL getZipUrl() {
		return PdfGeneration.class.getResource("/pdfGeneration_windows.zip");
	}

	@Override
	public List<String> getCommandArguments() {
		final List<String> commandArguments = new ArrayList<>(7);

		commandArguments.add("cmd");
		commandArguments.add("/c");
		commandArguments.add("start");
		commandArguments.add("node\\node.exe");
		commandArguments.add("printToPdf.js");

		return commandArguments;
	}

}