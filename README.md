# PDF Generator - Java PDF Generation Library

This is a Java [Puppeteer](https://github.com/GoogleChrome/puppeteer) wrapper library. It comes with Nodejs and Chromium binaries for Linux and Windows and extracts them into 
operating system's preferred `temp` directory. This extraction path can be overridden via `PDF_GENERATION_TOOL_PATH` environment variable.

Currently not working!

# Usage

See [Demo Class](https://bitbucket.org/kodgemisi/pdf-generator-core/src/master/src/main/java/com/example/Demo.java) for example usage.

## Adding as dependency

```xml
<repositories>
  <repository>
      <id>jitpack.io</id>
      <url>https://jitpack.io</url>
  </repository>
</repositories>

<dependency>
    <groupId>org.bitbucket.kodgemisi</groupId>
    <artifactId>pdf-generator-windows</artifactId>
    <version>${pdf-generator-windows.version}</version>
</dependency>
```

Don't forget to add `<pdf-generator-windows.version>x.y.z</pdf-generator-windows.version>` to your `<properties>` section in your `pom.xml` file.

See https://jitpack.io/#org.bitbucket.kodgemisi/pdf-generator-windows for available versions and further Jitpack information. 

# LICENSE

© Copyright 2018 Kod Gemisi Ltd.

Mozilla Public License 2.0 (MPL-2.0)

https://tldrlegal.com/license/mozilla-public-license-2.0-(mpl-2)

MPL is a copyleft license that is easy to comply with. You must make the source code for any of your changes available under MPL, but you can combine the MPL software with proprietary code, as long as you keep the MPL code in separate files. Version 2.0 is, by default, compatible with LGPL and GPL version 2 or greater. You can distribute binaries under a proprietary license, as long as you make the source available under MPL.

[See Full License Here](https://www.mozilla.org/en-US/MPL/2.0/)